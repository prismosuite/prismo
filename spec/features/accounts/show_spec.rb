# frozen_string_literal: true

require 'rails_helper'

feature 'Browsing user profile' do
  let(:account_page) { Accounts::ShowPage.new }
  let(:account_comments_page) { Accounts::CommentsPage.new }

  let(:user) { create(:user, :with_account) }
  let!(:story) { create(:activitypub_post, account: user.account) }
  let!(:comment) { create(:activitypub_comment, account: user.account) }

  scenario 'guest user browses other user account homepage' do
    account_page.load(username: user.account.username)

    expect(account_page).to be_displayed
    expect(account_page).to have_stories

    expect(account_page.stories.length).to eq 1
  end

  scenario 'guest user browses other user account comments page' do
    account_page.load(username: user.account.username)
    account_page.sub_nav.comments_link.click

    expect(account_comments_page).to be_displayed
    account_comments_page.wait_for_comments
    expect(account_comments_page).to have_comments

    expect(account_comments_page.comments.length).to eq 1
  end
end
