# frozen_string_literal: true

require 'rails_helper'

describe Stories::Update do
  let(:user) { create(:user, :with_account) }
  let(:story) { create(:activitypub_post, :link, account: user.account) }

  let(:args) do
    {
      account: user.account,
      name: 'Sample title',
      tag_list: 'foo, bar',
      story: story,
      url: story.url,
      content_source: story.content_source
    }
  end

  describe '#run' do
    subject { described_class.run(args) }

    context 'when params are fully valid' do
      it { is_expected.to be_valid }

      it 'updates modified_at column' do
        expect { subject }.to change(story, :modified_at)
      end

      context 'when edit grace period has passed' do
        before do
          story.update(created_at: Time.current - 4.minutes)
        end

        it 'updates modified_count column' do
          expect { subject }.to change(story, :modified_count).by(1)
        end
      end

      context 'when edit grace period has not passed' do
        before do
          story.update(created_at: Time.current - 2.minutes)
        end

        it 'updates modified_count column' do
          expect { subject }.to_not change(story, :modified_count)
        end
      end
    end

    context 'when actor is admin' do
      let(:user) { create(:user, :with_account, :admin) }

      it 'is possible to update story url' do
        args[:url] = 'https://changed.com'
        expect { subject }.to change(story, :url).to 'https://changed.com'
      end

      it 'is possible to update title even when title update limit is exceed' do
        args[:name] = 'Changed title'
        expect { subject }.to change(story, :name).to 'Changed title'
      end
    end

    context 'when actor is a regular user' do
      it 'is not possible to update story url' do
        args[:url] = 'https://changed.com'
        expect { subject }.to_not change(story, :url)
      end

      it 'is not possible to update title when title update limit is exceed' do
        story.update(created_at: 65.minutes.ago)
        args[:title] = 'Changed title'

        expect(subject).to_not be_valid
      end
    end
  end
end
