# frozen_string_literal: true

require_relative '../../sections/navbar_section'
require_relative '../../sections/story_row_section'

class DashboardsHotPage < SitePrism::Page
  set_url '/dashboard'
  set_url_matcher %r{\/dashboard?\z}

  section :navbar, ::NavbarSection, '.main-header'
  sections :stories, ::StoryRowSection, '.story-row'
end
