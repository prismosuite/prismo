require_relative 'base_page'

class Search::CommentsPage < Search::BasePage
  set_url '/search/comments'
  set_url_matcher %r{\/search\/comments}

  sections :comments, ::CommentSection, '.comment'
end
