# frozen_string_literal: true

require_relative '../../sections/navbar_section'
require_relative '../../sections/comments_tree_section'
require_relative '../../sections/story_row_section'

module Stories
  class ShowPage < SitePrism::Page
    set_url '/posts{/id}'
    set_url_matcher %r{\/posts\/[a-zA-Z0-9-]+\z}

    element :vote_btn, '.story-row__score-btn'

    section :navbar, NavbarSection, '.main-header'
    section :comments_tree, CommentsTreeSection, '[data-controller="comments-tree"]'
    sections :stories, StoryRowSection, '.story-row'

    section :new_comment_form, '#new_comment' do
      element :comment_body_input, '#comment_body'
    end

    def toggle_vote
      vote_btn.click
    end
  end
end
