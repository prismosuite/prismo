# frozen_string_literal: true

class NavbarSection < SitePrism::Section
  element :add_story_link, 'a', text: '+ Add story'
  element :comments_link, 'a', text: 'Comments'

  element :user_menu_toggle,
          '.main-header__profile a[data-target="dropdown.toggle"]'

  element :sign_out_link, 'a', text: 'Sign-out'

  def open_user_dropdown
    user_menu_toggle.click
  end

  def sign_out
    sign_out_link.click
  end
end
