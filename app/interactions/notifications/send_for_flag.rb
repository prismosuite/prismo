# frozen_string_literal: true

class Notifications::SendForFlag < ActiveInteraction::Base
  object :flag

  def execute
    User.admins.each do |recipient|
      CreateNotificationJob.call(
        'new_flag',
        author: flag.actor,
        recipient: recipient.account,
        notifable: flag,
        context: flag.flaggable
      )
    end
  end
end
