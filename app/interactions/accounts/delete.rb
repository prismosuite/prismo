class Accounts::Delete < ActiveInteraction::Base
  object :account
  string :current_password

  def execute
    if account.user.valid_password?(current_password)
      Accounts::SuspendJob.perform_later account.id, true
    else
      errors.add(:current_password, I18n.t('accounts.errors.invalid_password'))
    end
  end
end
