# frozen_string_literal: true

module ActivityPub
  def self.table_name_prefix
    'activitypub_'
  end
end
