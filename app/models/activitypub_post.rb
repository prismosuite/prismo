class ActivityPubPost < ActivityPubObject
  include RailsSettings::Extend
  Gutentag::ActiveRecord.call(self)

  attr_accessor :tag_list

  pg_search_scope :search, against: { name: 'A', content: 'B' },
                           using: { tsearch: { prefix: true } }

  has_many :comments, dependent: :destroy, class_name: :ActivityPubComment, foreign_key: :parent_id
  belongs_to :url_meta, optional: true
  belongs_to :account, counter_cache: :stories_count, optional: true

  validates :url, allow_blank: true, uniqueness: true

  delegate :thumb, :thumb_url, :thumb_data?,
           to: :url_meta,
           allow_nil: true

  def article?
    !url.present?
  end

  def link?
    url.present?
  end

  def object_type
    article? ? :article : :page
  end
end
