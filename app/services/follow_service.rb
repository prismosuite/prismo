# frozen_string_literal: true

class FollowService
  def initialize(actor, target_object)
    @actor = actor
    @target_object = target_object
  end

  def call
    return if actor.following?(target_object) || actor.requested_follow?(target_object)

    target_object.locked? ? request_follow! : direct_follow!
  end

  private

  attr_reader :actor, :target_object

  def request_follow!
    follow_request = FollowRequest.create!(follower: actor,
                                           following: target_object)

    if target_object.local?
      CreateNotificationJob.call('follow_requested', author: actor,
                                                     recipient: target_object,
                                                     notifable: follow_request)
    end

    follow_request
  end

  def direct_follow!
    follow = actor.follow!(target_object)

    if target_object.local? && target_object_notifable?
      CreateNotificationJob.call('new_follower', author: actor,
                                                 recipient: target_object)
    end

    follow
  end

  def target_object_notifable?
    target_object.is_a?(Account)
  end
end
