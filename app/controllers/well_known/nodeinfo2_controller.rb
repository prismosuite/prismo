# frozen_string_literal: true

module WellKnown
  class Nodeinfo2Controller < ActionController::Base
    def show
      render json: {
        links: [
          { rel: 'http://nodeinfo.diaspora.software/ns/schema/2.0' },
          { href: nodeinfo2_real_url }
        ]
      }
    end

    def show_real
      render json: Nodeinfo2Serializer.new
    end
  end
end
