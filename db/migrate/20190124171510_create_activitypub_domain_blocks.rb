class CreateActivityPubDomainBlocks < ActiveRecord::Migration[5.2]
  def change
    create_table :activitypub_domain_blocks do |t|
      t.string :domain, null: false, default: ''
      t.integer :severity, default: 0, null: false

      t.timestamps
    end
    add_index :activitypub_domain_blocks, :domain, unique: true
  end
end
