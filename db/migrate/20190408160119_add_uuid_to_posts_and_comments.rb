class AddUuidToPostsAndComments < ActiveRecord::Migration[5.2]
  def change
    add_column :stories, :uuid, :uuid, index: true, default: 'uuid_generate_v4()'
    add_column :comments, :uuid, :uuid, index: true, default: 'uuid_generate_v4()'
  end
end
