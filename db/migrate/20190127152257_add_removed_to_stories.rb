class AddRemovedToStories < ActiveRecord::Migration[5.2]
  def change
    add_column :stories, :removed, :boolean, default: false
  end
end
