FROM registry.gitlab.com/prismosuite/base-images:production

ARG UID=991
ARG GID=991

COPY Gemfile Gemfile.lock package.json yarn.lock /prismo/

RUN bundle config build.nokogiri --with-iconv-lib=/usr/local/lib --with-iconv-include=/usr/local/include \
 && bundle install -j$(getconf _NPROCESSORS_ONLN) --deployment --without test development \
 && yarn install --pure-lockfile --ignore-engines \
 && yarn cache clean

RUN addgroup -g ${GID} prismo && adduser -h /prismo -s /bin/sh -D -G prismo -u ${UID} prismo \
 && mkdir -p /prismo/public/system /prismo/public/assets /prismo/public/packs /prismo/public/uploads \
 && chown -R prismo:prismo /prismo/public

COPY . /prismo

RUN chown -R prismo:prismo /prismo

VOLUME /prismo/public/system

USER prismo

RUN SECRET_KEY_BASE=precompile_placeholder bundle exec rails webpacker:compile

ENTRYPOINT ["/sbin/tini", "--"]
